import React from "react";
import ReactDOM from "react-dom";
import Routers from "./routers.js";
import "./config/starter";
import GlobalStyled from "./assets/globalstyled";

ReactDOM.render(
  <React.Fragment>
    <>
      <Routers />
      <GlobalStyled />
    </>
  </React.Fragment>,
  document.getElementById("root")
);
 