import styled from "styled-components";

const FooterContainer = () => {
  return (
    <Footer className="d-none d-md-block">
      <h2>Footer</h2>
    </Footer>
  );
};

export default FooterContainer;

const Footer = styled.footer`
  background-color: #f39c12;
`;

// ## advanced
// const Footer = styled.footer.attrs({
//   className: "d-none d-md-block",
// })`
//   background-color: #f39c12;
// `;
