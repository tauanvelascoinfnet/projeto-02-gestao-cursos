import styled from "styled-components";
import NavBar from "../navbar/index";
const HeaderContainer = () => {
  return (
    <Header>
      <NavBar />
    </Header>
  );
};

export default HeaderContainer;

const Header = styled.header`
  background-color: #f39c12;
`;
