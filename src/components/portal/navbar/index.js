import React, { useState } from "react";
import {
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
  Collapse,
} from "reactstrap";
import { Link } from "@reach/router";
import styled from "styled-components";
import { sizeDevice } from "../../../util/devices";
const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar light expand="md">
      <Container>
        <Logo href="/">reactstrap</Logo>
        <NavbarToggler onClick={toggle} />
        <Menu isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink tag={Link} to="/">
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/cursos">
                Cursos
              </NavLink>
            </NavItem>
          </Nav>
        </Menu>
      </Container>
    </Navbar>
  );
};

export default NavBar;

const Logo = styled(NavbarBrand)`
  flex: 1;
`;
const Menu = styled(Collapse)`
  @media (min-width: ${sizeDevice.tablet}) {
    flex: 0;
  }
`;
