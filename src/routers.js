import { Router } from "@reach/router";

import Home from "./views/home";
import Cursos from "./views/cursos";
import Layout from "./components/portal/layout";

//errors
// import page404 from "./views/error/404";

const Routers = () => (
  <Layout>
    <Router>
      <Home path="/" />
      <Cursos path="/cursos" />
    </Router>
  </Layout>
);

export default Routers;
